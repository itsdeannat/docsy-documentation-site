# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.1.0...v2.0.0) (2022-03-04)


### ⚠ BREAKING CHANGES

* testing this breaking change

See merge request itsdeannat/docsy-documentation-site!6

* Merge branch 'testing-breaking-change' into 'main' ([14a9eba](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/14a9ebaea82eda152f8658f97a067c05750a5861))

## [1.1.0](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.9...v1.1.0) (2022-03-04)


### Features

* Testing a feature ([c9a94cf](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/c9a94cfec267679333e034c502309469d1c09651))


### Bug Fixes

* Testing a fix ([6d9c15e](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/6d9c15e67e3bce2437947e9738d1154d2cb0dc03))

### [1.0.9](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.8...v1.0.9) (2022-03-04)

### [1.0.8](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.7...v1.0.8) (2022-03-04)


### Bug Fixes

* Another change ([93e45d0](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/93e45d0bf5a7318f595d2dc185356bc7695a0b69))


### Chores

* A change ([b58456a](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/b58456aff75a9e2b36f2c99bbba3d1dc3c20db5d))

### [1.0.7](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.6...v1.0.7) (2022-03-04)


### Chores

* Making a change ([2eac6e1](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/2eac6e118e121172c07065360f531c685fd4813c))

### [1.0.6](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.5...v1.0.6) (2022-03-04)

### [1.0.5](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.4...v1.0.5) (2022-02-24)


### Documentation Updates

* Update contribution guidelines ([1c54189](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/1c54189dcd0c06b48e72de65d75d1824fc313357))

### [1.0.4](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.3...v1.0.4) (2022-02-18)


### Chores

* Add .versionrc file ([c9009ca](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/c9009ca47bb2cd1615834d9f1049ebd137d65d66))

### [1.0.3](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.2...v1.0.3) (2022-02-18)

### [1.0.2](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.1...v1.0.2) (2022-02-18)


### Bug Fixes

* Readd information to concepts homepage ([41b7c28](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/41b7c28c719dc8d1f313a4b53190cd38f8d6996e))

### [1.0.1](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v1.0.0...v1.0.1) (2022-02-18)

## [1.0.0](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v0.1.2...v1.0.0) (2022-02-18)


### Features

* Update concepts homepage ([1bd44ae](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/1bd44aec433f6bbda34e590338785e3568e7491b))

### [0.1.2](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v0.1.1...v0.1.2) (2022-02-18)


### Features

* Update reference homepage ([40a10e4](https://gitlab.com/itsdeannat/docsy-documentation-site/commit/40a10e4e09849f5669e10739141a8824a4d549fb))

### [0.1.1](https://gitlab.com/itsdeannat/docsy-documentation-site/compare/v0.1.1-0...v0.1.1) (2022-02-18)

### 0.1.1-0 (2022-02-18)
